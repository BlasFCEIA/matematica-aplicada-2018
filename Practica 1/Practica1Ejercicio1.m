%1.  Asuma que se encuentran definidas en memoria las siguientes variables:
%v=[5; 0; 4; 5; -2; 1; 7]   x=[4, 1]   y=[2  5]   z=[3 0 1; 1 2 6; 0 -1 7]
%Sin usar MATLAB, indique qu ?e resultados se obtienen al ejecutar los siguientes comandos:
%(a) min(v(2:5:7))
%(b) size(z�)
%(c) ones(x)
%(d) x*y�
%(e) y*y
%(f) x�*y
%(g) z*v(1:3)
%(h) [z v]
%(i) z(y)
%(j) sum(y + 2)


v=[5; 0; 4; 5; -2; 1; 7]   
x=[4, 1]   
y=[2  5]   
z=[3 0 1; 1 2 6; 0 -1 7]

min(v(2:5:7)) % El minimo del vector v, partiendo desde la posici�n 2 hasta la 7, sumando 5 unidades
              % Osea, el m�nimo entre 0 y 7 (v(2) y v(2+5), o mejor escrito
              % v(2) y v(7)
              
size(z')      % El tama�o de z transpuesta

ones(x)       % Un vector columna de 1, la cantidad de 1 est� dada por el primer elemento de x

x*y'          % El producto escalar como lo conocemos de algebra 1, pero aplico transpuesta por el producto de matrices

y*y           % Error, uno de los dos debe estar transpuesto para dar el m�dulo al cuadrado del vector y
y*y'          % Eso da el m�dulo al cuadrado del vector y

x'*y          % Una matriz 2x2 donde la primer fila es x(1)y(1), x(1)y(2) y la segunda fila es x(2)y(1), x(2)y(2) 

z*v(1:3)      % Un vector columna de dimensi�n 3, donde cada componente es el valor del producto escalar entre
              % la fila de la matriz z y el segmento del vector v
              % v(1:3) = [5 0 4]
              %  z1 = [3 0 1]
              %  z2 = [1 2 6]
              %  z3 = [0 -1 7]

              %  5*3 + 0*0 + 4*1 = 19
              %  5*1 + 0*2 + 4*6 = 29
              %  5*0 + 0*-1 + 4*7 = 28

[z v]         % Error, por las dimensiones, el vector v es mucho mas grande que las filas de la matriz z, pero si lo recortamos
              % obtendr�amos una matriz ampliada
[z v(1:3)]    % Una matriz ampliada, donde la �ltima columna, es el vector v.

z(y)          % Es un vector fila, con la dimensi�n de y, cada valor, est� dado por el indice que indica la componente de y
              % recorre a la matriz por columna, es decir, una matriz de
              % 3x3 tiene 9 elementos, el elemento 1 es el 1x1, el elemento
              % 2 es el de la columna 1 fila 2, el elemento 3 es el de la
              % columna 1 fila 3, el elemento 4 es el de la columna 2 fila 1 y as� hasta recorrer toda la matriz
              % en este caso, el vector ser� [1 2], por el 1 de la columna
              % 1 y el 2 de la columna 2, ya que y = [2 5]

sum(y + 2)    % A cada elemento de y se le suma 2 y luego, se suman todos los valores del vector
              % es decir y + 2 = [2+2 5+2] y luego, sum(y + 2) = 2+2+5+2 = 11